import React from 'react'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {createStackNavigator} from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import Login from './Isi/login'
import Contact from './Isi/contactus'
import Home from './Isi/home'
import Details from './Isi/details'
import Details2 from './Isi/details2'
import Details3 from './Isi/details3'

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name = "Home" component= {Home} />
        {/* <Drawer.Screen name = "Details" component= {Details} /> */}
        <Drawer.Screen name = "Contact Us" component= {Contact} />
        <Drawer.Screen name = "Logout" component= {Login} />
    </Drawer.Navigator>
)


export default () => (
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name = "Login" component={Login} />
            <Stack.Screen name = "Home" component={DrawerScreen} />
            <Stack.Screen name = "Homescreen" component={Details} />
            <Stack.Screen name = "Task" component={Details2} />
            <Stack.Screen name = "Movies" component={Details3} />


        </Stack.Navigator>
    </NavigationContainer>
)
