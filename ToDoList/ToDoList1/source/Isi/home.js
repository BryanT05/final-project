import { StatusBar } from 'expo-status-bar';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { MaterialIcons } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import React from 'react';
import {index} from '../index';
import List from './list';
import { StyleSheet, Text, View, ImageBackground, Image, Dimensions, TextInput, ScrollView, TouchableOpacity , Linking} from 'react-native';

const screen = Dimensions.get('window'); 

const image = { uri: "https://iphone7papers.com/wp-content/uploads/papers.co-so00-pastel-green-blue-blur-gradation-33-iphone6-wallpaper-250x444.jpg" };
export default class Home extends React.Component {
  constructor(props){
    super(props);
    this.state= {
      noteArray:[],
      noteText:'',
    }
  }

  addNote(){
    if(this.state.noteArray){
      var d = new Date();
      this.state.noteArray.push({
        'date': d.getFullYear() +
        '/' + (d.getMonth() +1) +
        '/' + d.getDate(),
        'note':this.state.noteText,
      });
      this.setState({ noteArray: this.state.noteArray});
      this.setState({noteText:''})
    }
  }

deleteNote(key){
    this.state.noteArray.splice(key,1);
    this.setState({noteArray:this.state.noteArray})
}
  render() {
    let list = this.state.noteArray.map((val,key) =>{
      return <List key ={key} keyval={key} val ={val}
              deleteMethod = {()=> this.deleteNote(key)} />
    })
    return (
      <ImageBackground source={image} style={styles.image}>
        <TouchableOpacity onPress={this.props.navigation.openDrawer}>
          <View style={styles.NavBar}>
            <MaterialIcons name="view-headline" size={30} color="black" />
            <Text style={{ marginLeft: 230, marginTop: 15}}>Selamat Datang!</Text>
          </View>
        </TouchableOpacity>
        <Text style={{ fontSize: 20, marginLeft: 300}}>Bagus FE</Text>
        <View style={styles.containerText}>
        <MaterialIcons name="format-list-bulleted" size={24} color="black" />
          <Text style={{fontSize: 25}}> Daftar List</Text>
        </View>
        <ScrollView>
          <View style = {{justifyContent:'center',alignItems:'center',marginTop:30}}>
          <TouchableOpacity onPress = {() => (this.props.navigation.navigate("Homescreen"))}>
            <View style = {{height:50,width:200,borderWidth:2,borderRadius:50,marginBottom:30,justifyContent:'center',alignItems:'center',backgroundColor:'black'}}>
              <Text style = {{fontSize:30,fontWeight:'500',color:'white'}}>Home</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => (this.props.navigation.navigate("Task"))}>
            <View style = {{height:50,width:200,borderWidth:2,borderRadius:50,marginBottom:30,justifyContent:'center',alignItems:'center',backgroundColor:'black'}}>
              <Text style = {{fontSize:30,fontWeight:'500',color:'white'}}>Task</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => (this.props.navigation.navigate("Movies"))}>
            <View style = {{height:50,width:200,borderWidth:2,borderRadius:50,marginBottom:30,justifyContent:'center',alignItems:'center',backgroundColor:'black'}}>
              <Text style = {{fontSize:30,fontWeight:'500',color:'white'}}>Movies</Text>
            </View>
          </TouchableOpacity>
          </View>
          {/* {list} */}
        </ScrollView>
        <View style={styles.footerhead}>
        <View style={styles.containerLocation}>
          <TouchableOpacity onPress={this.addNote.bind(this)}>
            <Image style={styles.logoLoc} source={require("./image/list.png")}/>
          </TouchableOpacity>
        </View>
        <View style = {styles.footer}>
                    <TextInput
                        style= {styles.textInput}
                        onChangeText={(noteText) => this.setState({noteText})}
                        placeholder = 'Input a List...'
                        placeholderTextColor = 'white'
                        underlineColorAndroid = 'transparent'>
                    </TextInput>
            </View>
        </View>
        <View style={styles.containerFooter}>
          <Text>&copy; Copyright 2020 | To Do App</Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  NavBar: {
    flexDirection: 'row',
    marginTop: 40,
    marginLeft: 18
  },
  containerText: {
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerLocation: {
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerFooter: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContactUs: {
    paddingLeft : 25,
    paddingRight : 25,
    paddingTop : 50,
  },
  containerSocialWA: {
    padding: 15,
    marginRight: 0,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  containerEmail: {
    padding: 10,
    marginTop: 5,
    marginRight: 180,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  containerCredit1: {
    padding: 10,
    marginRight: 220,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  containerCredit2: {
    padding: 10,
    marginTop: 5,
    marginRight: 235,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  image: {
    flex: 1,
    justifyContent: "center",
    opacity: 1
  },
  
  Title: {
    fontSize: 25,
    // fontFamily: "AppleSDGothicNeo-SemiBold",
    color: "black",
    marginTop: 40,
  },
  textTitle: {
    fontSize: 20,
    // fontFamily: "AppleSDGothicNeo-SemiBold",
    color: "black",
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  textSocials: {
      fontSize: 20,
      color: "white",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
  },
  logo: {
    width: screen.width / 3.85, 
    height: screen.width / 4.25,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  logoLoc: {
    width: screen.width / 5, 
    height: screen.width / 5,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  textInput: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold'
  },
  footer:{
    flex: 1,
    marginBottom:20,
    paddingLeft: 30,
    // paddingRight: 30,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-start',
    backgroundColor: 'black',
    opacity:0.5,
    // backfaceVisibility:0.3,
  },
  footerhead: {
    flexDirection: "row",
    paddingLeft: 30,
    paddingRight: 30,
  }
});
