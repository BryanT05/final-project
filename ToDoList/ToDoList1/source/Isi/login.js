import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, Dimensions, TextInput, ScrollView, TouchableOpacity } from 'react-native';

const screen = Dimensions.get('window'); 

const image = { uri: "https://images.unsplash.com/photo-1518976024611-28bf4b48222e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=632&q=80" };
export default class Login extends React.Component {
  state = {
    username: 'Admin',
    password: '123456'
  };

  render() {
    return (
      <ImageBackground source={image} style={styles.image}>
      <ScrollView>
      <View style={styles.containerLogo}>
        <Image style={styles.logo} source={require('./image/logo.png')}/>
        <StatusBar style="auto" />
        <Text style={styles.textTitle}>Halaman Login</Text>
      </View>
      <View style={styles.containerText}>
        <Text style={styles.text}>Username / Email</Text>
        <TextInput style={styles.textInput} placeholder="Masukkan Username..." onChangeText={(value) => this.setState({username: value})}
        value={this.state.username}/> 
        <Text style={styles.text}>Password</Text>
        <TextInput style={styles.textInput} placeholder="Masukkan Password..." onChangeText={(value) => this.setState({password: value})}
        value={this.state.password} secureTextEntry={true} password={true}/>
      </View>
      <View style={styles.containerText2}>
        <TouchableOpacity onPress={() => 
            {
                if(this.state.username === 'Admin' && this.state.password === '123456'){
                  this.props.navigation.navigate("Home");
                // ToastAndroid.show('Invalid UserName',ToastAndroid.SHORT);
                return;
            }
                if(this.state.username.localeCompare('Admin')!=0){
                    alert("Wrong username");
                    // ToastAndroid.show('Invalid UserName',ToastAndroid.SHORT);
                    return;
                }

                if(this.state.password.localeCompare('123456')!=0){
                    alert("Wrong password");
                    // ToastAndroid.show('Invalid Password',ToastAndroid.SHORT);
                    return;
                }

                //Handle LOGIN

            }
        }>
          <Text style={styles.textButton} >MASUK</Text>
        </TouchableOpacity>
        <Text style={styles.text}>Belum punya akun ?</Text>
        <TouchableOpacity>
          <Text style={styles.textButton}>DAFTAR</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.containerFooter}>
        <Text>&copy; Copyright 2020 | To Do App</Text>
      </View>

      </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  containerLogo: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 100
  },
  containerText: {
    flex: 1,
    flexDirection: 'column',
    margin:50,
    
    
  },
  containerText2: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerFooter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 90
  },
  image: {
    flex: 1,
    justifyContent: "center",
    opacity: 1

  },
  textTitle: {
    fontSize: 25,
    // fontFamily: "AppleSDGothicNeo-SemiBold",
    color: "black",
    marginTop: 70,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  text: {
    fontSize: 15,
    marginTop: 20,
    margin: 5,
  },
  textButton: {
    padding: 10,
    textAlign:'center',
    backgroundColor:'#68a0cf',
    borderRadius: 20,
    borderWidth: 4,
    borderColor: '#68a0cf'
  },
  logo: {
    width: screen.width / 2.75, 
    height: screen.width / 3.25,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  }, 
  textInput: {
    height: 40, 
    borderColor: 'black', 
    borderWidth: 1,
    paddingRight: 10,
    paddingLeft: 10,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // elevation: 5,
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});
