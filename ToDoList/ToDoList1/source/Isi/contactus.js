import { StatusBar } from 'expo-status-bar';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, Dimensions, Linking, TextInput, ScrollView, TouchableOpacity } from 'react-native';

const screen = Dimensions.get('window'); 

const image = { uri: "https://i.ibb.co/w4TBJyq/photo-2020-08-14-14-50-00.jpg" };

export default class ContactUs extends React.Component {
  render() {
    return (
      <ImageBackground source={image} style={styles.image}>
      
        <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
          <View style={styles.NavBar}>
            <Ionicons name="md-arrow-round-back" size={40} color="black" />
          </View>
        </TouchableOpacity>
        <View style={styles.containerText}>
          <Image style={styles.logo} source={require("./image/logo.png")}/>
        </View>
        <View style={styles.containerContactUs}>
          <Text style={styles.textTitle}>CONTACT US</Text>
        </View>
        <TouchableOpacity onPress={() => Linking.openURL('http://google.com')}>
          <View style={styles.containerSocialWA}>
            <MaterialCommunityIcons name="whatsapp" size={24} color="white" />
            <Text style={styles.textSocials}> | +62812800080</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Linking.openURL('http://google.com')}>
          <View style={styles.containerEmail}>
            <MaterialCommunityIcons name="email" size={24} color="white" />
            <Text style={styles.textSocials}> | help@todo.com</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.containerContactUs}>
          <Text style={styles.textTitle}>CREDITS</Text>
        </View>
        <TouchableOpacity onPress={() => Linking.openURL('http://google.com')}>
          <View style={styles.containerCredit1}>
            <MaterialCommunityIcons name="github-circle" size={24} color="white" />
            <Text style={styles.textSocials}> | @BryanT05</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Linking.openURL('http://github.com/bagusfe')}>
          <View style={styles.containerCredit2}>
          <MaterialCommunityIcons name="github-circle" size={24} color="white" />
            <Text style={styles.textSocials}> | @Bagusfe</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.containerLocation}>
        <Text style={styles.textTitle}>FIND US</Text>
          <TouchableOpacity onPress={() => Linking.openURL('http://maps.google.com')}>
            <Image style={styles.logoLoc} source={require("./image/loc.png")}/>
          </TouchableOpacity>
          <Text>DKI Jakarta, Jakarta Barat, Cengkareng</Text>
          <Text>Kelurahan Salak, 15560</Text>
        </View>
        <View style={styles.containerFooter}>
          <Text>&copy; Copyright 2020 | To Do App</Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  NavBar: {
    flexDirection: 'row',
    marginTop: 40,
    marginLeft: 18
  },
  containerText: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerLocation: {
    margin: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerFooter: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContactUs: {
    paddingLeft : 25,
    paddingRight : 25,
    paddingTop : 50,
  },
  containerSocialWA: {
    padding: 10,
    marginRight: 180,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  containerEmail: {
    padding: 10,
    marginTop: 5,
    marginRight: 180,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  containerCredit1: {
    padding: 10,
    marginRight: 220,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  containerCredit2: {
    padding: 10,
    marginTop: 5,
    marginRight: 235,
    marginLeft: 25,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  },
  image: {
    flex: 1,
    justifyContent: "center",
    opacity: 1
  },
  
  Title: {
    fontSize: 25,
    // fontFamily: "AppleSDGothicNeo-SemiBold",
    color: "black",
    marginTop: 40,
  },
  textTitle: {
    fontSize: 20,
    // fontFamily: "AppleSDGothicNeo-SemiBold",
    color: "black",
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  textSocials: {
      fontSize: 20,
    //   fontFamily: "AppleSDGothicNeo-SemiBold",
      color: "white",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
  },
  logo: {
    width: screen.width / 2.75, 
    height: screen.width / 3.25,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  logoLoc: {
    width: screen.width / 3.75, 
    height: screen.width / 4.25,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  }
});
