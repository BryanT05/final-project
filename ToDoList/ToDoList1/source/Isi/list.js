import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import {index} from '../index';
export default class List extends React.Component {
  render(){
    return(
      <TouchableOpacity onPress={() => this.props.navigation.navigate("Details")}>
      <View key = {this.props.keyval} style = {styles.containerSocialWA}>
        <Text style={styles.noteText}>{this.props.val.date}</Text>
        <Text style={styles.noteText}>{this.props.val.note}</Text>
        <TouchableOpacity onPress={this.props.deleteMethod} style = {styles.noteDelete}>
          <MaterialIcons name="delete" size={24} color="white" />
        </TouchableOpacity>
      </View>
      </TouchableOpacity>
    )
  }  
};

const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    borderBottomWidth: 2,
    // borderBottomColor: '#ededed',
    borderRadius:100,
    borderWidth:2,
    margin:20,
    paddingRight:20,
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: 'white',
    color: 'white'
  },
  noteDelete: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    padding:10 ,
    top: 10,
    bottom: 10,
    right: 10
  },
  noteDeleteText: {
    color: 'blue',
  },
  containerSocialWA: {
    height: 70,
    width:350,
    padding: 15,
    marginTop: 10,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#3c3c3c",
    backgroundColor: "#3c3c3c",
    flexDirection: "row",
  }
});

