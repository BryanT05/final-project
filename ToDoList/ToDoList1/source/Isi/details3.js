import React from 'react'
import {Text,View, StyleSheet,TouchableOpacity,ScrollView,TextInput} from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
import { Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 
import { FontAwesome } from '@expo/vector-icons'; 
import Note from './note'
import { MaterialIcons } from '@expo/vector-icons';

const windowHeight = Dimensions.get('window').height;

export default class Details extends React.Component{
    
    constructor(props){
        super(props);
        this.state= {
          noteArray:[],
          noteText:'',
        }
      }

      addNote(){
        if(this.state.noteArray){
          var d = new Date();
          this.state.noteArray.push({
            'date': d.getFullYear() +
            '/' + (d.getMonth() +1) +
            '/' + d.getDate(),
            'note':this.state.noteText,
          });
          this.setState({ noteArray: this.state.noteArray});
          this.setState({noteText:''})
        }
      }
    
    deleteNote(key){
        this.state.noteArray.splice(key,1);
        this.setState({noteArray:this.state.noteArray})
    }
    
    render(){
        let notes = this.state.noteArray.map((val,key) =>{
            return <Note key ={key} keyval={key} val ={val}
                    deleteMethod = {()=> this.deleteNote(key)} />
          })
        return(
            <View style = {styles.container}>
                <LinearGradient colors={['#EA6464', '#6EA4C2']} style={styles.gradient}/>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")}>
                     <View style = {styles.iconAtas}>
                     <Ionicons name="md-arrow-round-back" size={40} color="black" />
                </View>
                </TouchableOpacity>  
                <View style = {styles.home}>
                  <MaterialIcons name="personal-video" size={24} color="black" />
                    <Text style = {{fontSize:40,fontWeight:'bold'}}> Movies</Text>
                </View>
                {/* <View style = {styles.body}> */}
                    <ScrollView>
                        {notes}
                    </ScrollView>
                {/* </View> */}
                <View style = {styles.footer}>
                    <TouchableOpacity  onPress={this.addNote.bind(this)}>
                        <Entypo name="add-to-list" size={30} color="white" style={{padding:10, paddingLeft: 30}}/>
                    </TouchableOpacity>
                    <TextInput
                        style= {styles.textInput}
                        onChangeText={(noteText) => this.setState({noteText})}
                        placeholder = 'Add A Task'
                        placeholderTextColor = 'white'
                        underlineColorAndroid = 'transparent'>
                    </TextInput>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    gradient:{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: windowHeight,
    },
    iconAtas:{
        paddingTop: 30,
        paddingLeft: 10,
        height:70,
        // borderWidth:2,
    },
    home:{
        height:70,
        borderWidth:2,
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'center',
        marginLeft: 20,
        marginTop: 20,
        marginRight: 20,
        borderRadius: 10,
    },
    body:{
        flex:1
    },
    footer:{
        height:50,
        // borderWidth:2,
        marginBottom:30,
        flexDirection:'row',
        alignItems:'center',
        // justifyContent:'flex-start',
        backgroundColor: 'black',
        opacity:0.5,
        // backfaceVisibility:0.3,
    },
    Add: {
        fontSize:50,
        color:'white', 
        padding : 10,
        
    },
    textInput: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold'
        // margin: 50
        // padding: 20,
        // backgroundColor: '#c4c4c4',
        // borderTopWidth: 2,
        // borderTopColor: '#ededed',
        // opacity:0.3,
      },
})