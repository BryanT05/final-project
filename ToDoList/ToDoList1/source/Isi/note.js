import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

export default class Note extends React.Component {
  render(){
    return(
      <View key = {this.props.keyval} style = {styles.note}>
        <Text style={styles.noteText}>{this.props.val.date}</Text>
        <Text style={styles.noteText}>{this.props.val.note}</Text>
        <TouchableOpacity onPress={this.props.deleteMethod} style = {styles.noteDelete}>
          <MaterialIcons name="delete" size={24} color="black" />
        </TouchableOpacity>
  </View>
    )
  }  
};

const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    borderBottomWidth: 2,
    // borderBottomColor: '#ededed',
    borderRadius:100,
    borderWidth:2,
    margin:20,
    paddingRight:20,
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: 'white',
  },
  noteDelete: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    padding:10 ,
    top: 10,
    bottom: 10,
    right: 10
  },
  noteDeleteText: {
    color: 'blue',
  }
});

