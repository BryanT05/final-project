# Final project

#Final Project IMRN0720
## Bryan Tamin
## Bagus Frayoga E
Aplikasi ini adalah suatu aplikasi To Do List yang akan membantu anda mengingat apa saja yang seharusnya anda kerjakan
aplikasi ini dibuat karena orang sering lupa apa saja tugas mereka.

## Login Credential <br>
Username : Admin<br>
Password : 123456

Spesifikasi: 
- Menggunakan React Navigation 5 (Drawer dan Stack)
- Menggunakan React Native Expo CLI
- Belum menggunakan API 

## Link File 
- <a href="https://www.figma.com/file/jW0eNhJQpB1sIGFM4ehU2n/Home?node-id=0%3A1">Mockup Figma</a>
- <a href="https://drive.google.com/drive/folders/1JXHhf0DhHaAY9aKgiBW5Csh-6kmdxkUM?usp=sharing">Google Drive</a>
